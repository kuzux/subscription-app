# subscription-app

This project is basically written to teach me stripe and try to get as clean a rails project as possible. It has pretty good test coverage, 
so I believe the project will run fine if the tests pass. You can also see generate/view the api docs via yard. For rest api doumentation, see the controller
documentations in the yard docs.

The main bit of the project is that there are users that create publications and subscribe/unsubscribe. The subscribed users can read
other users' publications. This was the simplest application I could think of where a subscription system would  make sense.

It depends on stripe api and also needs a jwt signing key in the application credentials. It uses sqlite as a database. I haven't tried deploying it yet 
but will update the readme with deployment instructions once I do.
