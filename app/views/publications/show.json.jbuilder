# frozen_string_literal: true

json.call(@pub, :id, :title, :contents, :user_id)
