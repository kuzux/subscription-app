# frozen_string_literal: true

json.publications @pubs.each do |pub|
    json.call(pub, :id, :title)
end
