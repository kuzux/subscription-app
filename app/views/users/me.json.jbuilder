# frozen_string_literal: true

json.call(@current_user, :id, :email, :stripe_customer_id)
json.subscription do
    json.exists @current_user.subscription.exists?
    json.end_date @current_user.subscription.end_date
end

json.profile do
    json.full_name @current_user.profile.full_name
    json.profile_picture_url @current_user.profile.profile_picture_url
end
