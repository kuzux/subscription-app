# typed: true
# frozen_string_literal: true

# Handles the things required in response to the stripe webhook
class StripeEventProcessor
    # @return [User, nil] the associated user if the hook accessed it
    attr_reader :user

    # @param event [Stripe::Event] The webhhok event to process
    def initialize(event)
        @event = event
        @user = nil
    end

    # Handles the stripe event by dispatching on its type, ignoring if there are no handlers for its type
    # @return [Boolean] true if the event was dispatched, false if it was ignored
    def handle_event!
        case event_type
        when 'invoice.payment_succeeded'
            handle_payment_succeeded(@event.data.object)

            # TODO: Handle invoice.payment_failed to notify the user on subscription renewal failures
        else
            return false
        end

        true
    end

    private

    def handle_payment_succeeded(event_body)
        customer_id = event_body.customer
        @user = User.by_customer_id(customer_id)

        return unless event_body.lines.data[0].type == 'subscription'

        end_timestamp = event_body.lines.data[0].period.end
        end_time = Time.at(end_timestamp)
        @user.subscription.renew!(till: end_time)
    end

    def event_type
        @event.type
    end
end
