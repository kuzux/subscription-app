# typed: true
# frozen_string_literal: true

# Parses and verifies an event sent for the stripe webhook
class StripeEventParser
    # @return [Stripe::Event, nil] the parsed event for the request (or nil if there was an error)
    attr_reader :event

    # @return [Exception, nil] The error for the parser (or nil if no error is found)
    attr_reader :error

    # @param request [ActionDispatch::Request] The request object for the controller action
    def initialize(request)
        @request = request
        @error = nil
        @event = nil
    end

    # Performs the operation by parsing the request body and verifying the signature using `Rails.application.credentials[:stripe_webhook_secret]`
    # and populates the error and event fields
    def parse!
        payload = @request.raw_post
        sig_header = @request.headers['HTTP_STRIPE_SIGNATURE']
        begin
            webhook_secret = Rails.application.credentials[:stripe_webhook_secret]
            @event = Stripe::Webhook.construct_event(payload, sig_header, webhook_secret)
        rescue StandardError => e
            @error = e
            @event = nil
        end
    end

    # @return [Boolean] true if there was no error during parse
    def contains_error?
        !error.nil?
    end
end
