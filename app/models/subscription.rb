# typed: true
# frozen_string_literal: true

# Represents a subscription belonging to a user in the system. Note that none of the methods modify the database
# In the current data model, a user can have one (and exactly one) subscription. The subscription might or might
# not be present at the current time
class Subscription
    extend T::Sig

    GRACE_PERIOD = 3.days

    # @return [User] The user who owns this subscription
    attr_reader :user

    # @param user [User] who this subscription belongs_to
    def initialize(user)
        @user = user
    end

    # Renews the subscription until the given time (plus a grace period)
    # @param till [DateTime] When to renew the subscription until.
    def renew!(till:)
        user.subscribed_until = till + GRACE_PERIOD
    end

    # Cancels the subscription, effective immediately
    def cancel!
        user.subscribed_until = nil
    end

    # @return [Boolean] true if the subscription is active at the current time
    def exists?
        return true if user.is_admin?
        return false if user.subscribed_until.nil?

        Time.now <= user.subscribed_until
    end

    # @return [DateTime] until what time is the subscription active
    def end_date
        user.subscribed_until
    end
end
