# typed: true
# frozen_string_literal: true

# Represents a user for the systems (with login, payment and core functionalities)
# Its password is stored hashed in the database (via `has_secure_password`) and the payments are
# mostly delegated to Stripe
class User < ApplicationRecord
    has_secure_password
    has_one_attached :avatar

    has_many :publications

    # Signs up as a user (updating the database). Notably does not check the database for duplicate emails
    # @param email [String] the email for the signing up user
    # @param password [String] the password for the user
    # @param full_name [String, nil] The full name for the user
    # @return [User] the newly created user
    def self.signup!(email:, password:, full_name:)
        create!(id: SecureRandom.uuid, email: email, password: password, fullname: full_name)
    end

    # @param id [String] the user id (a UUID) to search in the database
    # @return [User, nil] The user with the specified id in the database, or nil if the user is not found
    def self.by_id(id)
        find_by(id: id)
    end

    # @param email [String] the user email to search in the database
    # @return [User, nil] The user with the specified email in the database, or nil if the user is not found
    def self.by_email(email)
        find_by(email: email)
    end

    # @param customer_id [String] the user's stripe customer id to search in the database
    # @return [User, nil] The user with the specified customer id in the database, or nil if the user is not found
    # @see #stripe_customer
    def self.by_customer_id(customer_id)
        find_by(stripe_customer_id: customer_id)
    end

    # @return [AuthToken] the auth token for the user (starting from the current time)
    def to_token
        AuthToken.from_user(self)
    end

    # @return [Boolean] true if the user is currently subscribed
    # @see #subscription
    def subscribed?
        subscription.exists?
    end

    # @return [Subscription] The subscription object for the user (not nil whether the user is subscribed or not)
    def subscription
        @subscription ||= Subscription.new(self)
    end

    # Turns the user into an admin
    def make_admin!
        self.is_admin = true
    end

    # Calls the stripe api to create a new customer
    # @return [Stripe::Customer] the created customer object
    def create_stripe_customer!
        res = Stripe::Customer.create({ email: email })
        self.stripe_customer_id = res.id
        res
    end

    # Returns the associated stripe customer object for the user. Always performs a stripe API call. (A retrieve
    # customer call if an existing customer is stored for the user, amd a create customer call if not)
    # @return [Stripe::Customer] The associated stripe customer object for the user.
    def stripe_customer
        if stripe_customer_id.nil?
            create_stripe_customer!
        else
            Stripe::Customer.retrieve(T.must(stripe_customer_id))
        end
    end

    # @return [Profile] the profile for the user
    def profile
        @profile ||= Profile.new(self)
    end
end
