# typed: true
# frozen_string_literal: true

class Profile
    attr_reader :user

    def initialize(user)
        @user = user
    end

    def full_name
        @user.fullname
    end

    def add_profile_picture!(file)
        @user.avatar.attach(file)

        @user.avatar.attached?
    end

    def profile_picture_url
        return nil if @user.avatar.nil? || !@user.avatar.attached?

        Rails.application.routes.url_helpers.rails_blob_path(@user.avatar, only_path: true)
    end
end
