# typed: true
# frozen_string_literal: true

# Represents a JWT token, including its signature and associated user
class AuthToken
    attr_reader :jwt_payload

    def initialize(jwt_payload, subject)
        @jwt_payload = jwt_payload
        @subject = subject
    end
    private_class_method :new

    # Verifies (with `Rails.application.credentials[:jwt_secret]`) and constructs a token from a base 64 encoded token
    # @param encoded_jwt [String] the jwt token to decode and verify
    # @return [AuthToken, nil] The resulting token object or nil if the verification failed
    def self.from_encoded(encoded_jwt)
        res = JWT.decode(encoded_jwt, Rails.application.credentials[:jwt_secret], true)
        new(res[0], nil)
    rescue JWT::VerificationError, JWT::ExpiredSignature
        nil
    end

    # Constructs a token for a given user. The token is set to expire 60 minutes after the current time
    # @param user [User] the user
    # @return [AuthToken] the token constructed for the user
    def self.from_user(user)
        exp = Time.now.to_i + 60 * 60
        payload = { iss: '',
                    exp: exp,
                    aud: '',
                    sub: user.id,
                    roles: ['user'] }
        new(payload, user)
    end

    # Encodes and signs (with application's jwt secret) an auth token
    # @return [String] The encoded and signed token
    def encode
        JWT.encode @jwt_payload, Rails.application.credentials[:jwt_secret]
    end

    # @return [String] The subject id given in the token payload (the user id, ideally)
    def subject_id
        @jwt_payload['sub']
    end

    # @return [DateTime] The expiration time from the token payload
    def expiration_time
        @jwt_payload['exp']
    end

    # @return [User] The user with the given subject id in the token payload
    def subject
        @subject ||= User.by_id(subject_id)
    end
end
