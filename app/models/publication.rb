# typed: true
# frozen_string_literal: true

# Represents an article/post a user can publish and a subscriber can view on the system
class Publication < ApplicationRecord
    belongs_to :user

    # Creates a new publication on the db with the given user as author
    # @param user [User] the author of the publication
    # @param title [String] the title of the publication
    # @param contents [String] the publication body
    # @return [Publication] the newly created publication on the db
    def self.create_for_user!(user:, title:, contents:)
        create!(id: SecureRandom.uuid, user_id: user.id, title: title, contents: contents)
    end

    # @param user [User] the usre wishing to read the the publication
    # @return [Boolean] true if the publication can be read by the user
    def can_be_read?(user)
        user_id == user.id || user.is_admin? || user.subscribed?
    end

    # @param user [User] the usre wishing to write to the the publication
    # @return [Boolean] true if the publication can be written to by the user
    def can_be_written?(user)
        user_id == user.id || user.is_admin?
    end

    # @param id [String] the id of the post to find in the database
    # @return [Publication, nil] The publication with given id, or nil if such a publication is not found in the database
    def self.by_id(id)
        find_by(id: id)
    end

    # @param size [Integer] The maximum number of featured publications to fetch and return from the database
    # @return [Array<Publication>] The featured publications in the database
    def self.featured(size)
        order('random()').limit(size)
    end
end
