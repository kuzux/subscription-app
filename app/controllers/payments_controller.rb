# typed: false
# frozen_string_literal: true

class PaymentsController < ApplicationController
    before_action :authenticate!, only: 'subscribe'
    def subscribe
        return render json: {}, status: :conflict if current_user.subscribed?

        payment_method_id = params.require(:payment_method_id)
        price_id = Rails.application.config.x.stripe_price_id
        customer_id = current_user.stripe_customer.id
        current_user.save # in case we generated a new stripe customer

        begin
            attach_payment_method(customer: customer_id, payment_method: payment_method_id)
        rescue Stripe::CardError => e
            return render json: { error: e.message }
        end

        subscription = Stripe::Subscription.create(
            customer: customer_id,
            items: [{ price: price_id }],
            expand: ['latest_invoice.payment_intent']
        )

        render json: subscription.to_json
    end

    def stripe_webhook
        parser = StripeEventParser.new(request)
        parser.parse!

        return render json: { error: parser.error.message }, status: 400 if parser.contains_error?

        proc = StripeEventProcessor.new(parser.event)
        proc.handle_event!
        # For the db changes the processor might have done
        proc.user&.save!

        render json: { status: :success }, status: 200
    end

    private

    def attach_payment_method(customer:, payment_method:)
        Stripe::PaymentMethod.attach(payment_method, customer: customer)

        Stripe::Customer.update(customer, invoice_settings: {
                                    default_payment_method: payment_method
                                })
    end
end
