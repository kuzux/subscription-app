# typed: false
# frozen_string_literal: true

# Controller for user related endpoints (signup, login etc)
class UsersController < ApplicationController
    before_action :authenticate!, only: %w[me upload_profile_picture]

    # POST /signup
    # Create a new user. The full_name parameter is optional
    # @example Sign up succesfully
    #   POST /signup
    #   { "email": "test@example.com", "password": "123456", "full_name": "John Doe"}
    #   # => HTTP 204
    # @example Missing password argument
    #  POST /signup
    #  { "email": "test@example.com" }
    #  # => HTTP 400
    # @example Already an existing user
    #   POST /signup
    #   { "email": "test@example.com", "password": "987654"}
    #   # => HTTP 409
    def signup
        params.require(:email)
        params.require(:password)
        params.permit(:full_name)

        return render status: :conflict, json: {} unless User.by_email(params[:email]).nil?

        User.signup!(email: params[:email], password: params[:password], full_name: params[:full_name])
    end

    # POST /login
    # Get the authentication token to use in further requests. Return 401 if there was no user or the password was incorrect
    # @example Successful Login
    #  POST /login
    #  { "email": "test@example.com", "password": "123456" }
    #  # => { "token": "eyJhbGciO..." }
    # @example Login Failure
    #   POST /login
    #   { "email": "test@example.com", "password": "nope" }
    #   # => HTTP 401
    # @see ApplicationController#authenticate!
    def login
        params.require(:email)
        params.require(:password)

        user = User.by_email(params[:email])&.authenticate(params[:password])
        return render json: {}, status: :unauthorized unless user

        render json: { "token": AuthToken.from_user(user).encode }
    end

    # GET /me
    # Simplest authenticated endpoint. Simply returns the current user as json
    # @example Succesful invocation
    #   GET /me
    #   # => {
    #     "id":"3022ec01-b0e9-some-uuid",
    #     "email":"test@example.com",
    #     "stripe_customer_id":"cus_asdfgh",
    #     "subscription": { "exists":true, "end_date":"2021-05-03T07:11:25.000Z" }
    #   }
    def me; end

    # POST /users/profile-picture
    # @example
    #   POST /users/profile-picture
    #   profile_picture=<FILE>
    def upload_profile_picture
        pic = params.require(:profile_picture)
        # TODO: Require mime types
        # TODO: Handle auto-resizing of large images
        current_user.profile.add_profile_picture!(pic)
    end
end
