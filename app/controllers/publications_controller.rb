# typed: true
# frozen_string_literal: true

# Controller for publication-related endpoints
class PublicationsController < ApplicationController
    before_action :authenticate!

    # GET /publications
    # Returns all of the publications. Only accessible if the current user is an admin
    def index
        return render status: :forbidden, json: {} unless current_user.is_admin?
    end

    # GET /publications/featured
    # Returns a list of 10 featured publications. Only the publication titles are returned
    def featured
        @pubs = Publication.featured(10)
    end

    # POST /publications/
    # Create a new publication for the current user
    # @example
    #   POST /publications/
    #   { "title" => "test", "contents" => "bla bla bla" }
    #   # => HTTP 204
    def create
        params.require(:title)
        params.require(:contents)

        @pub = Publication.create_for_user!(user: current_user, title: params[:title], contents: params[:contents])
    end

    # GET /publications/:id
    # Returns a publication (both title and body). Returns 403 if the user is not authorized to view the publication (i.e. the user is not subscribed)
    def show
        @pub = Publication.by_id(params[:id])

        return render status: :not_found, json: {} if @pub.nil?
        return render status: :forbidden unless @pub.can_be_read?(current_user)
    end

    # DELETE /publications/:id
    # Deletes the specified publication if the user is allowed to delete it
    # @example Succesful
    #   DELETE /publications/some-uuid
    #   # => HTTP 204
    # @example Unsuccesful
    #   DELETE /publications/some-other-uuid
    #   # => HTTP 403
    # @example Publication does not exist
    #   DELETE /publications/nonex-istent
    #   # => HTTP 404
    def destroy
        pub = Publication.by_id(params[:id])
        return render status: :not_found, json: {} if pub.nil?
        return render status: :forbidden, json: {} unless pub.can_be_written?(current_user)

        pub.destroy!
    end
end
