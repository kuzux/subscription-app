# typed: true
# frozen_string_literal: true

# Base class for all cotrollers in the application
class ApplicationController < ActionController::API
    rescue_from ActionController::ParameterMissing do |e|
        render json: { error: e.message }, status: :bad_request
    end

    # Raise in a controller method to return a 401 responase
    class Unauthenticated < StandardError
        attr_reader :message

        def initialize(message)
            super
            @message = message
        end
    end

    rescue_from Unauthenticated do |e|
        render json: { error: e.message }, status: :unauthorized
    end

    # @return [AuthToken] The current token for authenticated endpoints
    attr_reader :current_token
    # @return [User] The current user specified in the token. Available only for authenticated endpoints
    attr_reader :current_user

    # Authenticates the current user, the authentication is performed via JWT
    # The POST /login endpoint returns a JWT, then for each aiuthenticated request requires an `Authorization` header
    # with the value `Bearer TOKEN`. If the token in the header is invalid, or expired, an HTTP 401 response is returned
    # @see UsersController#login
    def authenticate!
        header = request.headers['Authorization']
        raise Unauthenticated, 'Missing authorization header' unless header

        raw_token = header.split(' ').last
        @current_token = AuthToken.from_encoded(raw_token)
        raise Unauthenticated, 'Invalid (or expired) token' unless @current_token

        @current_user = @current_token.subject
        raise Unauthenticated, "Missing user #{@current_token.subject_id}" unless @current_user
    end
end
