# typed: true
class AddFullnameToUsers < ActiveRecord::Migration[6.0]
    def change
        add_column :users, :fullname, :text, null: true
    end
end
