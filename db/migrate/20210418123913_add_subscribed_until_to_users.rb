# typed: true
class AddSubscribedUntilToUsers < ActiveRecord::Migration[6.0]
    def change
        add_column :users, :subscribed_until, :datetime, precision: 6, null: true
    end
end
