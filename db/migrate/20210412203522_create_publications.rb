# typed: true
class CreatePublications < ActiveRecord::Migration[6.0]
    def change
        create_table :publications, id: :text do |t|
            t.references :user, type: :text, null: false, foreign_key: true
            t.text :title
            t.text :contents

            t.timestamps
        end
    end
end
