# typed: true
class AddStripeCustomerIdToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :stripe_customer_id, :text, null: true
  end
end
