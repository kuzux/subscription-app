# typed: false
# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Publications', type: :request do
    let(:user) { User.signup!(email: 'publication@example.com', password: '123456', full_name: nil) }
    let(:other_user) { User.signup!(email: 'publication@example.com', password: '123456', full_name: nil) }
    let(:admin) { User.signup!(email: 'publication@example.com', password: '123456', full_name: nil) }

    before do
        admin.make_admin!
        admin.save!
    end

    describe 'POST #create' do
        let(:default_params) { { title: 'test', contents: 'body test' } }

        it 'should create a new publication' do
            expect do
                post '/publications', params: default_params, headers: login_as(user)
            end.to change { Publication.count }.by(1)
        end

        it 'should create a new publication for the logged in user with the specified fields' do
            post '/publications', params: default_params, headers: login_as(user)

            id = body_json[:id]
            pub = Publication.by_id(id)

            expect(pub.title).to eq('test')
            expect(pub.contents).to eq('body test')
            expect(pub.user_id).to eq(user.id)
        end

        it 'should not create a publication without a title or a body' do
            expect do
                post '/publications', params: { title: 'no body' }, headers: login_as(user)
            end.not_to change { Publication.count }
            expect(response).to have_http_status(:bad_request)

            expect do
                post '/publications', params: { contents: 'no title' }, headers: login_as(user)
            end.not_to change { Publication.count }
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe 'DELETE #destroy' do
        it 'should delete a publication' do
            pub = Publication.create_for_user!(user: user, title: 'delete test', contents: 'whatever')
            expect do
                delete "/publications/#{pub.id}", headers: login_as(user)
            end.to change { Publication.count }.by(-1)
        end

        it 'should not delete a nonexistent publication' do
            expect do
                delete "/publications/#{SecureRandom.uuid}", headers: login_as(user)
            end.not_to change { Publication.count }

            expect(response).to have_http_status(:not_found)
        end

        it "should not delete somebody else's publication" do
            pub = Publication.create_for_user!(user: other_user, title: 'delete test', contents: 'whatever')
            expect do
                delete "/publications/#{pub.id}", headers: login_as(user)
            end.not_to change { Publication.count }

            expect(response).to have_http_status(:forbidden)
        end

        it "should delete somebody else's publication if we're an admin" do
            pub = Publication.create_for_user!(user: user, title: 'delete test', contents: 'whatever')
            expect do
                delete "/publications/#{pub.id}", headers: login_as(admin)
            end.to change { Publication.count }.by(-1)
        end
    end

    describe 'GET #show' do
        let(:publication) { Publication.create_for_user!(user: user, title: 'show test', contents: 'whatever') }

        it 'should show our own publication' do
            get "/publications/#{publication.id}", headers: login_as(user)

            expect(body_json[:id]).to eq(publication.id)
            expect(body_json[:title]).to eq(publication.title)
            expect(body_json[:contents]).to eq(publication.contents)
            expect(body_json[:user_id]).to eq(publication.user_id)
        end

        it 'should return not found for a nonexistent publication' do
            get "/publications/#{SecureRandom.uuid}", headers: login_as(user)

            expect(response).to have_http_status(:not_found)
        end

        it "should not show somebody else's publication if we are not subscribed" do
            get "/publications/#{publication.id}", headers: login_as(other_user)

            expect(response).to have_http_status(:forbidden)
        end

        it "should show somebody else's publication if we are subscribed" do
            other_user.subscription.renew!(till: 1.month.from_now)
            other_user.save!

            get "/publications/#{publication.id}", headers: login_as(other_user)

            expect(response).to have_http_status(:success)
        end

        it "should show somebody else's publication if we are an admin" do
            get "/publications/#{publication.id}", headers: login_as(admin)

            expect(response).to have_http_status(:success)
        end
    end

    describe 'GET #index' do
        it 'should return forbidden if we are not an admin' do
            get '/publications/', headers: login_as(user)

            expect(response).to have_http_status(:forbidden)
        end

        # TODO: What to do when we are an admin? Pagination?
    end

    describe 'GET #featured' do
        it 'should only show a limited number of publications' do
            20.times { Publication.create_for_user!(user: user, title: 'featured test', contents: 'whatever') }

            get '/publications/featured', headers: login_as(user)
            expect(body_json[:publications].length).to eq(10)
        end

        it 'should return publication titles but not bodies' do
            2.times { Publication.create_for_user!(user: user, title: 'featured test', contents: 'whatever') }

            get '/publications/featured', headers: login_as(user)
            expect(body_json[:publications][0]).to have_key(:title)
            expect(body_json[:publications][0]).not_to have_key(:contents)
        end

        pending 'should return all publications marked as featured in the database'
        pending 'should return a collection of links to the publications we can see'
    end
end
