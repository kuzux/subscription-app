# typed: false
# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Payments', type: :request do
    describe 'POST /subscribe' do
        let(:user) { User.signup!(email: 'publication@example.com', password: '123456', full_name: nil) }
        before do
            stripe_customer = double(Stripe::Customer, id: 'cus_test')
            allow(Stripe::Customer).to receive(:create)
                .and_return(stripe_customer)
            user.create_stripe_customer!
            user.save!

            # Mocking external dependency methods for a minimal working execution
            allow(Stripe::Customer).to receive(:retrieve).and_return(stripe_customer)
            allow(Stripe::PaymentMethod).to receive(:attach)
            allow(Stripe::Customer).to receive(:update)
            allow(Stripe::Subscription).to receive(:create)
        end

        it 'should try to attach the payment method to the customer' do
            expect(Stripe::PaymentMethod).to receive(:attach).with('pm_test', hash_including(customer: 'cus_test'))

            post '/payments/subscribe', params: { payment_method_id: 'pm_test' }, headers: login_as(user)
        end

        it 'should fail if attaching the payment method fails' do
            allow(Stripe::PaymentMethod).to receive(:attach).and_raise(Stripe::CardError.new('test failed', 999))

            post '/payments/subscribe', params: { payment_method_id: 'pm_test' }, headers: login_as(user)
            expect(response).to have_http_status(:success)
            expect(body_json[:error]).to eq('test failed')
        end

        it 'should make the payment method default for the customer' do
            expect(Stripe::Customer).to receive(:update).with('cus_test',
                                                              hash_including(invoice_settings: {
                                                                                 default_payment_method: 'pm_test'
                                                                             }))

            post '/payments/subscribe', params: { payment_method_id: 'pm_test' }, headers: login_as(user)
        end

        it 'should create a subscription and return it' do
            expect(Stripe::Subscription).to receive(:create).with(hash_including(customer: 'cus_test'))
                                                            .and_return({ test_field: 'dummy value' })
            post '/payments/subscribe', params: { payment_method_id: 'pm_test' }, headers: login_as(user)

            expect(body_json[:test_field]).to eq('dummy value')
        end

        it 'should not call stripe if the user is already subscribed' do
            user.subscription.renew!(till: 1.month.from_now)
            user.save

            expect(Stripe::Subscription).not_to receive(:create)
            post '/payments/subscribe', params: { payment_method_id: 'pm_test' }, headers: login_as(user)

            expect(response).to have_http_status(:conflict)
        end
    end

    describe 'POST /stripe-webhook' do
        def build_stripe_event(body)
            data = JSON.parse(body, symbolize_names: true)
            Stripe::Event.construct_from(data)
        end

        # try a real world-ish example, just mock the parser service
        it 'should route a payment succesful event to event processor correctly' do
            req_string = file_fixture('invoice.payment_succeeded.json').read

            # So we don't get any whitespace issues
            req_string = JSON.parse(req_string).to_json
            event = build_stripe_event(req_string)

            mock_svc = instance_double(StripeEventParser, event: event)
            expect(StripeEventParser).to receive(:new) do |req|
                # For some reason, req_string is interpreted as ascii here even though it is not
                expect(req.raw_post).to eq(req_string.force_encoding('UTF-8'))

                mock_svc
            end

            expect(mock_svc).to receive(:parse!)
            allow(mock_svc).to receive(:contains_error?).and_return(false)

            mock_proc = instance_double(StripeEventProcessor, handle_event!: true, user: nil)
            expect(StripeEventProcessor).to receive(:new).with(event).and_return(mock_proc)

            post '/payments/stripe-webhook', params: req_string,
                                             headers: { 'Content-Type' => 'application/json' }

            expect(response.status).to eq(200)
        end

        it 'should still succeed on an ignored event' do
            req_string = file_fixture('invoice.finalized.json').read

            # So we don't get any whitespace issues
            req_string = JSON.parse(req_string).to_json
            event = build_stripe_event(req_string)

            mock_svc = instance_double(StripeEventParser, event: event)
            expect(StripeEventParser).to receive(:new) do |req|
                # For some reason, req_string is interpreted as ascii here even though it is not
                expect(req.raw_post).to eq(req_string.force_encoding('UTF-8'))

                mock_svc
            end

            expect(mock_svc).to receive(:parse!)
            allow(mock_svc).to receive(:contains_error?).and_return(false)

            mock_proc = instance_double(StripeEventProcessor, handle_event!: false, user: nil)
            expect(StripeEventProcessor).to receive(:new).with(event).and_return(mock_proc)

            post '/payments/stripe-webhook', params: req_string,
                                             headers: { 'Content-Type' => 'application/json' }

            expect(response.status).to eq(200)
        end

        it 'should return status 400 if the parser service fails' do
            req_string = '{"msg": "yeah that\'ll fail"}'
            mock_svc = instance_double(StripeEventParser, event: nil, error: ArgumentError.new('No parse'),
                                                          contains_error?: true)
            expect(StripeEventParser).to receive(:new) do |req|
                expect(req.raw_post).to eq(req_string)

                mock_svc
            end

            expect(mock_svc).to receive(:parse!)

            post '/payments/stripe-webhook', params: req_string,
                                             headers: { 'Content-Type' => 'application/json' }

            expect(response.status).to eq(400)
        end
    end
end
