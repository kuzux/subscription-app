# typed: false
# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Users', type: :request do
    describe 'POST /signup' do
        it 'should create a user with legal parameters' do
            expect do
                post '/signup', params: { email: 'controller@test.com', password: '123456', full_name: 'Test User' }
            end.to change { User.count }.by(1)

            expect(response).to have_http_status(:success)
        end

        it 'should create a user with no full name as well' do
            expect do
                post '/signup', params: { email: 'controller@test.com', password: '123456' }
            end.to change { User.count }.by(1)

            expect(response).to have_http_status(:success)
        end

        it 'should fail to create a user without a password' do
            expect do
                post '/signup', params: { email: 'controller@test.com' }
            end.not_to(change { User.count })

            expect(response).to have_http_status(:bad_request)
        end

        it 'should fail to create a user with the same email as an existing user' do
            User.signup!(email: 'existing@user.com', password: '123456', full_name: nil)
            expect do
                post '/signup', params: { email: 'existing@user.com', password: '123456' }
            end.not_to change { User.count }

            expect(response).to have_http_status(:conflict)
        end
    end

    describe 'POST /login' do
        let(:user) { User.signup!(email: 'login@test.com', password: '123456', full_name: nil) }

        it 'should return unauthorized on wrong email' do
            post '/login', params: { email: 'nobody@test.com', password: user.password }

            expect(response).to have_http_status(:unauthorized)
        end

        it 'should return unauthorized on wrong password' do
            post '/login', params: { email: 'login@test.com', password: 'incorrect' }

            expect(response).to have_http_status(:unauthorized)
        end

        it 'should return a jwt token on successful login' do
            post '/login', params: { email: user.email, password: user.password }

            expect(response).to have_http_status(:ok)
            expect(body_json).to have_key(:token)
        end
    end

    describe 'GET /me' do
        let(:user) { User.signup!(email: 'me@test.com', password: '123456', full_name: 'Me me me') }
        let(:auth_header) { "Bearer #{AuthToken.from_user(user).encode}" }

        it 'should return self if logged in' do
            get '/me', headers: { 'Authorization' => auth_header }

            expect(response).to have_http_status(:ok)

            expect(body_json[:id]).to eq(user.id)
            expect(body_json[:email]).to eq(user.email)
            expect(body_json[:profile][:full_name]).to eq('Me me me')
        end

        it 'should return unauthorized if not logged in' do
            get '/me'

            expect(response).to have_http_status(:unauthorized)
        end

        it 'should return the subscription status of a new user' do
            get '/me', headers: { 'Authorization' => auth_header }

            expect(body_json[:subscription][:exists]).to eq(false)
            expect(body_json[:subscription][:end_date]).to eq(nil)
        end

        it 'should return the subscription status of a new user' do
            user.subscription.renew!(till: 1.month.from_now)
            user.save

            get '/me', headers: { 'Authorization' => auth_header }

            expect(body_json[:subscription][:exists]).to eq(true)
            expect(body_json[:subscription][:end_date]).to be >= 1.month.from_now
        end

        it 'should return the stripe customer id of the user' do
            allow(Stripe::Customer).to receive(:create).and_return(double(Stripe::Customer, id: 'req_test'))
            user.create_stripe_customer!
            user.save

            get '/me', headers: { 'Authorization' => auth_header }

            expect(body_json[:stripe_customer_id]).to eq('req_test')
        end

        it 'should return a profile picture url' do
            # TODO: this thing sometimes fails to attach the profile picture, not sure if it is due to fixture_file_upload or active storage
            # Though we know this only happens when we upload something for a user
            # And then try to fetch that user again from the database
            # Also, adding a sleep call doesn't work
            file = fixture_file_upload('profile-picture.png', 'image/png')
            user.profile.add_profile_picture!(file)

            get '/me', headers: { 'Authorization' => auth_header }

            expect(body_json[:profile][:profile_picture_url]).to match(%r{^/rails/active_storage/blobs/})
            expect(body_json[:profile][:profile_picture_url]).to match(%r{/profile-picture.png$})
        end
    end

    describe 'POST /users/profile-picture' do
        let(:user) { User.signup!(email: 'login@test.com', password: '123456', full_name: nil) }

        it 'should create an uploaded file' do
            file = fixture_file_upload('profile-picture.png', 'image/png')
            expect do
                post '/users/profile-picture', params: { profile_picture: file }, headers: login_as(user)
            end.to change { ActiveStorage::Attachment.count }.by(1)
        end
    end
end
