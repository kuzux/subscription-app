# typed: false
# frozen_string_literal: true

require 'rails_helper'

RSpec.describe StripeEventProcessor do
    describe 'invoice.payment_succeeded event' do
        let(:user) { User.new(email: 'service@test.com') }
        before do
            allow(User).to receive(:by_customer_id).and_return(user)
        end

        it 'should not renew subscription if the event is not about a subscription' do
            event_hash = {
                type: 'invoice.payment_succeeded',
                data: {
                    object: { customer: 'cus_mocked',
                              lines: { data: [{
                                  type: 'something else',
                                  other_attribute: 'whatever'
                              }] } }
                }
            }
            mock_event = RecursiveOpenStruct.new(event_hash, recurse_over_arrays: true)
            svc = StripeEventProcessor.new(mock_event)

            expect(user.subscription).not_to receive(:renew!)
            svc.handle_event!
        end

        it 'should renew the subscription until the given end date' do
            end_date = 1.month.from_now
            # discarding the microsecond part
            end_date = Time.at(end_date.to_i)

            event_hash = {
                type: 'invoice.payment_succeeded',
                data: {
                    object: { customer: 'cus_mocked',
                              lines: { data: [{
                                  type: 'subscription',
                                  period: { end: end_date.to_i }
                              }] } }
                }
            }
            mock_event = RecursiveOpenStruct.new(event_hash, recurse_over_arrays: true)
            svc = StripeEventProcessor.new(mock_event)

            expect(user.subscription).to receive(:renew!).with(till: end_date)
            svc.handle_event!
        end
    end

    it 'should return false on an ignored event' do
        event_hash = {
            type: 'invoice.finalized',
            data: {
                object: { customer: 'cus_mocked',
                          lines: { data: [{
                              type: 'something else',
                              other_attribute: 'whatever'
                          }] } }
            }
        }
        mock_event = RecursiveOpenStruct.new(event_hash, recurse_over_arrays: true)
        svc = StripeEventProcessor.new(mock_event)

        expect(svc.handle_event!).to eq(false)
    end
end
