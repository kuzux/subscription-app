# typed: false
# frozen_string_literal: true

require 'rails_helper'

RSpec.describe StripeEventParser do
    it 'should call the Stripe API with correct params (payload, signature, secret)' do
        mock_headers = { 'HTTP_STRIPE_SIGNATURE' => 'sig' }
        mock_req = instance_double(ActionDispatch::Request, raw_post: 'reqbody', headers: mock_headers)
        svc = StripeEventParser.new(mock_req)

        mock_credentials = { stripe_webhook_secret: 'secret' }
        allow(Rails.application).to receive(:credentials).and_return(mock_credentials)

        expect(Stripe::Webhook).to receive(:construct_event).with('reqbody', 'sig', 'secret').and_return(nil)
        svc.parse!
    end

    it 'should have the correct event and no error after a successful parse' do
        mock_req = instance_double(ActionDispatch::Request, raw_post: 'reqbody', headers: {})
        svc = StripeEventParser.new(mock_req)

        mock_event = instance_double(Stripe::Event)
        allow(Stripe::Webhook).to receive(:construct_event).and_return(mock_event)
        svc.parse!

        expect(svc.event).to eq(mock_event)
        expect(svc.error).to eq(nil)
        expect(svc.contains_error?).to eq(false)
    end

    it 'should have the correct error state after a parse error' do
        mock_req = instance_double(ActionDispatch::Request, raw_post: 'reqbody', headers: {})
        svc = StripeEventParser.new(mock_req)

        test_exc = StandardError.new('test exception')
        allow(Stripe::Webhook).to receive(:construct_event).and_raise(test_exc)

        svc.parse!

        expect(svc.event).to eq(nil)
        expect(svc.error).to eq(test_exc)
        expect(svc.contains_error?).to eq(true)
    end
end
