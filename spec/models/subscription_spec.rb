# typed: false
# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Subscription, type: :model do
    let(:user) { User.new(id: 'test-user', email: 'model@test.com', password: '123456') }
    let(:subscription) { Subscription.new(user) }

    it 'should not exist right after the user is created' do
        expect(subscription.exists?).to eq(false)
    end

    it 'should not have an end date after creation' do
        expect(subscription.end_date).to eq(nil)
    end

    it 'should update the end date after renewal' do
        subscription.renew!(till: 1.month.from_now)
        expect(subscription.end_date).to be >= 1.month.from_now
    end

    it 'should be considered existing if the user is an admin' do
        user.make_admin!
        expect(subscription.exists?).to eq(true)
    end

    it 'should exist after the initial renewal' do
        subscription.renew!(till: 1.month.from_now)
        expect(subscription.exists?).to eq(true)
    end

    it 'should still be considered exisyting one day after its end date' do
        subscription.renew!(till: 1.month.from_now)
        Timecop.freeze(Time.now + 1.month + 1.day) do
            expect(subscription.exists?).to eq(true)
        end
    end

    it 'should no longer be subscribed a week after the subscription end date' do
        subscription.renew!(till: 1.month.from_now)
        Timecop.freeze(Time.now + 1.month + 1.week) do
            expect(subscription.exists?).to eq(false)
        end
    end

    it 'should still be subscribed after the subscription was renewed' do
        subscription.renew!(till: 1.month.from_now)
        Timecop.freeze(Time.now + 1.month + 1.week) do
            subscription.renew!(till: 1.month.from_now)
            expect(subscription.exists?).to eq(true)
        end
    end

    it 'should no longer be subscribed after the subscription is cancelled' do
        subscription.renew!(till: 1.month.from_now)
        Timecop.freeze(Time.now + 1.week) do
            subscription.cancel!
            expect(subscription.exists?).to eq(false)
        end
    end
end
