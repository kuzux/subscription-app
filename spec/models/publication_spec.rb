# typed: false
# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Publication, type: :model do
    let(:user) { User.new(id: 'owner') }
    let(:other_user) { User.new(id: 'somebpdy-else') }
    let(:admin) { User.new(id: 'admin', is_admin: true) }

    let(:publication) { Publication.new(title: 'test post', contents: 'bla bla bla', user_id: user.id) }

    it 'can be written to by its owner' do
        expect(publication.can_be_written?(user)).to eq(true)
    end

    it "can't be written to by another user" do
        expect(publication.can_be_written?(other_user)).to eq(false)
    end

    it 'can be written to by an admin' do
        expect(publication.can_be_written?(admin)).to eq(true)
    end

    it 'can be read by its creator' do
        expect(publication.can_be_read?(user)).to eq(true)
    end

    it 'can be read by an admin' do
        expect(publication.can_be_read?(admin)).to eq(true)
    end

    it 'cannot be read by an unsubscribed user' do
        expect(publication.can_be_read?(other_user)).to eq(false)
    end

    it 'can be read by a subscribed user' do
        other_user.make_admin!
        expect(publication.can_be_read?(other_user)).to eq(true)
    end
end
