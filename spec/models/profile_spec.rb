# typed: false
# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Profile, type: :model do
    let(:user) { User.new(id: 'test-user', email: 'model@test.com', password: '123456', fullname: 'Profile Test') }
    let(:profile) { Profile.new(user) }

    it 'should have the full name of its user' do
        expect(profile.full_name).to eq('Profile Test')
    end

    it 'should have a null profile picture url after user creation' do
        expect(profile.profile_picture_url).to eq(nil)
    end

    it 'should have a non-null profile picture url after profile picture upload' do
        file = fixture_file_upload('profile-picture.png', 'image/png')
        profile.add_profile_picture!(file)

        expect(profile.profile_picture_url).not_to eq(nil)
    end
end
