# typed: false
# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AuthToken, type: :model do
    let(:user) { User.new(id: 'test-user', email: 'model@test.com', password: '123456') }

    it 'should generate a valid jwt (verifiably)' do
        token = AuthToken.from_user(user)

        expect do
            JWT.decode(token.encode, Rails.application.credentials[:jwt_secret], true)
        end.not_to raise_error

        expect do
            JWT.decode(token.encode, 'qweqwe', true)
        end.to raise_error(JWT::VerificationError)
    end

    it 'should have correct values for generated token' do
        token = AuthToken.from_user(user)
        decoded = JWT.decode(token.encode, Rails.application.credentials[:jwt_secret], true)[0]
        res = AuthToken.from_encoded(token.encode)

        expect(res.subject_id).to eq(decoded['sub'])
        allow(User).to receive(:by_id).with(user.id).and_return(user)
        expect(res.subject.id).to eq(decoded['sub'])
        expect(res.expiration_time).to eq(decoded['exp'])
    end

    it 'should generate a jwt with the given subject' do
        token = AuthToken.from_user(user)
        decoded = AuthToken.from_encoded(token.encode)

        expect(decoded.subject_id).to eq(user.id)
    end

    it 'should generate a jwt with correct expiration time' do
        expected_expiration = Time.now.to_i + 60 * 60
        token = AuthToken.from_user(user)
        decoded = AuthToken.from_encoded(token.encode)

        expect(decoded.expiration_time).to be <= expected_expiration
    end

    it 'should validate generated token' do
        token = AuthToken.from_user(user)
        res = AuthToken.from_encoded(token.encode)
        expect(res).not_to eq(nil)
    end

    it 'should fail to validate token generated with another key' do
        # We change the jwt secret to something else, generate a token with it and then change it back
        old_secret = Rails.application.credentials[:jwt_secret]
        mock_credentials = { jwt_secret: 'mocked' }
        allow(Rails.application).to receive(:credentials).and_return(mock_credentials)

        encoded = AuthToken.from_user(user).encode

        mock_credentials = { jwt_secret: old_secret }
        allow(Rails.application).to receive(:credentials).and_return(mock_credentials)

        res = AuthToken.from_encoded(encoded)
        expect(res).to eq(nil)
    end

    it 'should fail to validate expired token' do
        token = ''
        Timecop.freeze(5.hours.ago) do
            token = AuthToken.from_user(user)
        end

        res = AuthToken.from_encoded(token.encode)
        expect(res).to eq(nil)
    end
end
