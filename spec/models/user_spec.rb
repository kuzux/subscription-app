# typed: false
# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
    let(:user) { User.new(id: 'test-user', email: 'model@test.com', password: '123456', fullname: 'Model Test') }

    it 'should be unsubscribed at creation' do
        expect(user.subscribed?).to eq(false)
    end

    it 'should return a token with itself as the subject' do
        token = user.to_token
        expect(token.subject).to eq(user)
    end

    it 'should return a subscription for itself' do
        sub = user.subscription
        expect(sub.user).to eq(user)
    end

    it 'should create a stripe customer on first access to stripe_customer method' do
        expect(Stripe::Customer).to receive(:create)
            .with(hash_including(email: 'model@test.com'))
            .and_return(double(Stripe::Customer, id: 'cus_test'))
        cus = user.stripe_customer
        expect(cus.id).to eq('cus_test')
    end

    it 'should retrieve the created customer on subsequent accesses' do
        allow(Stripe::Customer).to receive(:create)
            .and_return(double(Stripe::Customer, id: 'cus_test'))
        user.create_stripe_customer!

        expect(Stripe::Customer).to receive(:retrieve).with('cus_test')
        user.stripe_customer
    end

    it 'should have the profile associated with itself' do
        expect(user.profile.user).to eq(user)
    end
end
