# typed: false
# frozen_string_literal: true

Rails.application.routes.draw do
    # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

    defaults format: :json do
        scope controller: :users, path: '/' do
            post 'signup' => :signup
            post '/login' => :login
            get '/me' => :me

            post '/users/profile-picture' => :upload_profile_picture
        end

        resources :publications, only: %i[index create show destroy] do
            collection do
                get '/featured' => :featured
            end
        end

        scope controller: :payments, path: '/payments' do
            post 'subscribe' => :subscribe
            post 'stripe-webhook' => :stripe_webhook
        end
    end
end
