# typed: strict
# frozen_string_literal: true

Stripe.api_key = Rails.application.credentials[:stripe_api_key]
# defined on stripe console, not a secret
Rails.application.config.x.stripe_price_id = 'price_1Ihba3Cm9FM1IdCwV0HEJx1W'
